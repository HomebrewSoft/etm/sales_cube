# -*- coding: utf-8 -*-
{
    "name": "Sales Cube",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://gitlab.com/HomebrewSoft.dev",
    "license": "LGPL-3",
    "depends": [
        "account",
        "product_prototype",
        "unitary_category",
        "product_pricelist_package",
        "sale_order_type",
    ],
    "data": [
        # security
        "security/ir.model.access.csv",
        # data
        # reports
        # views
        "views/account_move_line.xml",
        "views/mrp_production.xml",
        "views/product_template.xml",
        "views/sales_cube_wizard.xml",
        "views/res_partner.xml",
    ],
}
