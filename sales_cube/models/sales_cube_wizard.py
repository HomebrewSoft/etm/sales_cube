# -*- coding: utf-8 -*-
import base64
import csv
import collections
from datetime import datetime
from io import StringIO

from odoo import _, api, fields, models


def data_to_bytes(fieldnames, data):
    writer_file = StringIO()
    writer = csv.DictWriter(writer_file, fieldnames=fieldnames)
    writer.writeheader()
    writer.writerows(data)
    return writer_file.getvalue().encode("cp1252")


class SalesCubeWizard(models.TransientModel):
    _name = "sales.cube.wizard"

    start_date = fields.Date()
    end_date = fields.Date()
    data_file = fields.Binary()
    data_file_name = fields.Char()
    partner_id = fields.Many2one(
        comodel_name="res.partner",
    )


    def _get_invoices(self):
        sales_cube_domain = [
                    ("type", "=", ["out_invoice", "out_refund"]),
                    ("state", "=", "posted"),
                    ("invoice_date", ">=", self.start_date),
                    ("invoice_date", "<=", self.end_date),
                    ("deposit_invoice", "!=", True),
                ]
        if self.partner_id:
            sales_cube_domain.append(("partner_id", "=", self.partner_id.name))          
        invoices = self.env["account.move"].search(sales_cube_domain)
        return invoices

    def _get_data(self):
        invoices = self._get_invoices()
        lines = invoices.invoice_line_ids
        dep_lines = lines.filtered(lambda self: self.product_id.name != "ANTICIPO")
        usd_currency = self.env.ref("base.USD")
        mxn_currency = self.env.ref("base.MXN")

        data = [
            {
                "FolioFactura": line.move_id.name,
                "CodigoProducto": line.product_id.default_code,
                "Margen": line.margen_percentage,
                "MonedaFactura": line.move_id.currency_id.name,
                "ProductoDescCorta": line.product_id.name,
                "DescripcionProducto": line.name,
                "UnidadMedida": line.product_uom_id.name,
                "Cantidad": line.quantity,
                "CostoReposicion": line.product_id.quotation_cost,
                "CostoDirecto": line.product_id.standard_price,
                "PrecioUnitario": line.price_unit_converted,
                "PrecioUnitarioSinNC": line.price_unit,
                "DN": line.move_id.invoice_user_id.name,
                "EjecutivoSAC": line.move_id.partner_id.user_sac_id.name,
                "FUC": line.move_id.invoice_date,
                "Linea": line.product_id.print_type,
                "Division": line.move_id.team_id.name,
                "CodigoCliente": line.move_id.partner_id.ref,
                "UnitaryCategory": line.move_id.partner_id.unitary_category_id.name,
                "EjecutivoCotizador": line.move_id.partner_id.user_quotation_id.name,
                "RazonSocial": line.move_id.partner_id.name,
                "TipoCambioFacturacion": line.invoice_rate_date,
                "TipoCambioOficial": line.invoice_rate_date,
                "FechaUltimaCompra": line.last_date,
                "SubTotalPorProducto": line.SubTotalPorProducto,
                "SubTotalCtoRep": line.SubTotalCtoRep,
                "SubTotalCtoDir": line.SubTotalCtoDir,
                "MargenS/CosRep": line.MargenSCosRep,
                "MargenS/CosDir": line.MargenSCosDir,
                "Day": datetime.strftime(line.move_id.invoice_date, "%d"),
                "Month": datetime.strftime(line.move_id.invoice_date, "%m"),
                "Year": datetime.strftime(line.move_id.invoice_date, "%Y"),
                "TotalNotasDeCredito": line.credit_note,
            }
            for line in dep_lines
        ]
        data.append(
            {
                "SubTotalPorProducto": sum(row["SubTotalPorProducto"] for row in data),
                "SubTotalCtoRep": sum(row["SubTotalCtoRep"] for row in data),
                "SubTotalCtoDir": sum(row["SubTotalCtoDir"] for row in data),
                "MargenS/CosRep": sum(row["MargenS/CosRep"] for row in data),
                "MargenS/CosDir": sum(row["MargenS/CosDir"] for row in data),
            }
        )
        return data

    def _write_data(self, data):
        self.data_file_name = f"Sales Cube {self.start_date}/{self.end_date}.csv"
        self.data_file = base64.b64encode(data)

    def _download(self):
        # 'url': '/web/content/?model=sale.order&id={}&field=dummy&filename_field=dummy_filename&download=true'.format(
        #         self.id
        #     ),
        return {
            "type": "ir.actions.act_url",
            "url": f"/web/content/?model={self._name}&field=data_file&id={self.id}&filename={self.data_file_name}&download=true",
            "target": "self",
        }

    def _process_data(self, data):
        fieldnames = (
            "FolioFactura",
            "CodigoProducto",
            "Margen",
            "MonedaFactura",
            "ProductoDescCorta",
            "DescripcionProducto",
            "UnidadMedida",
            "Cantidad",
            "CostoReposicion",
            "CostoDirecto",
            "PrecioUnitario",
            "PrecioUnitarioSinNC",
            "DN",
            "EjecutivoSAC",
            "FUC",
            "Linea",
            "Division",
            "CodigoCliente",
            "UnitaryCategory",
            "EjecutivoCotizador",
            "RazonSocial",
            "TipoCambioFacturacion",
            "TipoCambioOficial",
            "FechaUltimaCompra",
            "SubTotalPorProducto",
            "SubTotalCtoRep",
            "SubTotalCtoDir",
            "MargenS/CosRep",
            "MargenS/CosDir",
            "Day",
            "Month",
            "Year",
            "TotalNotasDeCredito",
        )
        data_processed = data_to_bytes(fieldnames, data)
        return data_processed

    def download_data(self):
        data = self._get_data()
        data_processed = self._process_data(data)
        self._write_data(data_processed)
        return self._download()
