# -*- coding: utf-8 -*-
from odoo import api, fields, models


class ProductTemplate(models.Model):
    _inherit = "product.template"

    pending_orders_qty = fields.Float(string="Pending Orders", compute="_compute_pending_orders_qty")
    qty_dif = fields.Float(string="Diference", compute="_compute_qty_dif")

    def _compute_pending_orders_qty(self):
        for product in self:
            sale_order_lines = self.env["sale.order.line"].search([("product_id", "=", product.id)])
            product.pending_orders_qty = sum(sale_order_line.product_uom_qty - sale_order_line.qty_delivered for sale_order_line in sale_order_lines)

    @api.depends("qty_available")
    def _compute_qty_dif(self):
        for product in self:
            sale_order_lines = self.env["sale.order.line"].search([("product_id", "=", product.id)])
            product.qty_dif = product.qty_available - sum(sale_order_line.product_uom_qty - sale_order_line.qty_delivered for sale_order_line in sale_order_lines)
