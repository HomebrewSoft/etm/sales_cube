# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    margen = fields.Float(
        string="Margen",
        compute="_compute_margen",
    )
    margen_percentage = fields.Float(
        compute="_compute_margen_percentage",
    )
    total_replacement_cost = fields.Float(
        compute="_compute_total_replacement_cost",
    )
    price_unit_converted = fields.Float(
        compute="_compute_price_unit_converted",
    )
    last_date = fields.Date(
        compute="_compute_last_date",
    )
    credit_note = fields.Char(
        compute="_compute_credit_note",
    )
    SubTotalPorProducto = fields.Float(
        compute="_compute_SubTotalPorProducto",
    )
    SubTotalCtoRep = fields.Float(
        compute="_compute_SubTotalCtoRep",
    )
    SubTotalCtoDir = fields.Float(
        compute="_compute_SubTotalCtoDir",
    )
    MargenSCosRep = fields.Float(
        compute="_compute_MargenSCosRep",
    )
    MargenSCosDir = fields.Float(
        compute="_compute_MargenSCosDir",
    )
    invoice_rate_date = fields.Float(
        compute="_compute_invoice_rate_date",
    )

    def _compute_invoice_rate_date(self):
        for line in self:
            line.invoice_rate_date = 0
            for move_line in line.move_id.line_ids:
                if move_line.account_id.code in (
                    "101-105-100-000",
                    "101-105-201-001",
                    "101-105-301-001",
                ):
                    if move_line.amount_currency == 0:
                        line.invoice_rate_date = 1
                    else:
                        invoice_rate_date = (
                            move_line.debit + move_line.credit
                        ) / move_line.amount_currency
                        if invoice_rate_date < 0:
                            line.invoice_rate_date = -(invoice_rate_date)
                        else:
                            line.invoice_rate_date = invoice_rate_date

    def _get_invoice_rate(self):
        invoice_rate = 0
        for move_line in self.move_id.line_ids.filtered(
            lambda line: line.account_id.code
            in (
                "101-105-100-000",
                "101-105-201-001",
                "101-105-301-001",
            )
        ):
            if move_line.amount_currency == 0:
                invoice_rate = 1
            else:
                invoice_rate = (move_line.debit + move_line.credit) / move_line.amount_currency
                if invoice_rate < 0:
                    invoice_rate = -(invoice_rate)
            break
        return invoice_rate

    def _compute_MargenSCosDir(self):
        for line in self:
            if line.move_id.type == "out_refund":
                line.MargenSCosDir = -(line.SubTotalPorProducto - line.SubTotalCtoDir)
            else:
                line.MargenSCosDir = line.SubTotalPorProducto - line.SubTotalCtoDir

    def _compute_MargenSCosRep(self):
        for line in self:
            if line.move_id.type == "out_refund":
                line.MargenSCosRep = -(line.SubTotalPorProducto - line.SubTotalCtoRep)
            else:
                line.MargenSCosRep = line.SubTotalPorProducto - line.SubTotalCtoRep

    def _get_subtotalctodir(self):
        for line in self:
            subtotalctodir = 0
            thousands = line.quantity * line.product_uom_id.factor_inv
            if line.product_id.uom_id.factor_inv or line.product_id.uom_id.factor:
                quantity = thousands / (
                    line.product_id.uom_id.factor_inv or line.product_id.uom_id.factor
                )
                subtotalctodir = quantity * line.product_id.standard_price
            else:
                quantity = thousands / 1
                subtotalctodir = quantity * line.product_id.standard_price
        return subtotalctodir

    def _compute_SubTotalCtoDir(self):
        for line in self:
            subtotal = line._get_subtotalctodir()
            if line.move_id.type == "out_refund":
                line.SubTotalCtoDir = -(subtotal)
            else:
                line.SubTotalCtoDir = subtotal

    def _get_subtotalctorep(self):
        for line in self:
            subtotalctorep = 0
            thousands = line.quantity * line.product_uom_id.factor_inv
            if line.product_id.uom_id.factor_inv or line.product_id.uom_id.factor:
                quantity = thousands / (
                    line.product_id.uom_id.factor_inv or line.product_id.uom_id.factor
                )
                subtotalctorep = quantity * line.product_id.quotation_cost
            else:
                quantity = thousands / 1
                subtotalctorep = quantity * line.product_id.quotation_cost
        return subtotalctorep

    def _compute_SubTotalCtoRep(self):
        for line in self:
            subtotal = line._get_subtotalctorep()
            if line.move_id.type == "out_refund":
                line.SubTotalCtoRep = -(subtotal)
            else:
                line.SubTotalCtoRep = subtotal

    def _compute_SubTotalPorProducto(self):
        for line in self:
            invoice_rate = line._get_invoice_rate()
            if line.move_id.type == "out_refund":
                if invoice_rate == 1:
                    line.SubTotalPorProducto = -(line.price_subtotal)
                else:
                    line.SubTotalPorProducto = -(line.price_subtotal * invoice_rate)
            else:
                if invoice_rate == 1:
                    line.SubTotalPorProducto = line.price_subtotal
                else:
                    line.SubTotalPorProducto = line.price_subtotal * invoice_rate

    def _compute_credit_note(self):
        for line in self:
            dtype = line.move_id.type
            if dtype == "out_refund":
                cfdi = line.move_id.type_rel_cfdi_ids.id
                if cfdi:
                    line.credit_note = line.move_id.type_rel_cfdi_ids.move_name
                else:
                    line.credit_note = "NULL"
            else:
                line.credit_note = "NULL"

    def _compute_last_date(self):
        invoices = self.env["sales.cube.wizard"]._get_invoices()
        for line in self:
            previous_record = invoices.search(
                [
                    ("invoice_partner_display_name", "=", line.move_id.partner_id.name),
                    ("invoice_date", "<=", line.move_id.invoice_date),
                    ("name", "!=", line.move_id.name),
                ],
                limit=1,
                order="invoice_date DESC",
            )
            line.last_date = previous_record.invoice_date if previous_record else None

    @api.depends("price_unit")
    def _compute_price_unit_converted(self):
        for line in self:
            invoice_rate = line._get_invoice_rate()
            if invoice_rate == 1:
                line.price_unit_converted = line.price_unit
            else:
                line.price_unit_converted = line.price_unit * invoice_rate

    @api.depends("price_unit", "product_id.quotation_cost")
    def _compute_margen_percentage(self):
        for line in self:
            line.margen_percentage = 0
            if not (line.move_id and line.move_id.name and line.move_id.invoice_date):
                continue
            if line.move_id.currency_id == "MXN":
                line.margen_percentage = (
                    (line.price_unit - line.product_id.quotation_cost) / line.price_unit
                ) * 100
            else:
                currency = line.move_id.currency_id
                company_currency = line.move_id.company_id.currency_id
                converted_price_unit = currency._convert(
                    line.price_unit,
                    company_currency,
                    line.move_id.company_id,
                    line.move_id.invoice_date,
                )
                if converted_price_unit == 0:
                    converted_price_unit = 1
                    line.margen_percentage = (
                        (converted_price_unit - line.product_id.quotation_cost)
                        / converted_price_unit
                    ) * 100
                else:
                    line.margen_percentage = (
                        (converted_price_unit - line.product_id.quotation_cost)
                        / converted_price_unit
                    ) * 100

    @api.depends("price_subtotal", "product_id.standard_price", "quantity", "move_id.invoice_date")
    def _compute_margen(self):
        for line in self:
            invoice_rate = line._get_invoice_rate()
            if invoice_rate == 1:
                line.margen = line.price_subtotal - (line.product_id.standard_price * line.quantity)
            else:
                converted = line.price_subtotal * invoice_rate
                line.margen = converted - (line.product_id.standard_price * line.quantity)

    @api.depends("product_id.quotation_cost", "quantity", "move_id.invoice_date")
    def _compute_total_replacement_cost(self):
        usd = self.env.ref("base.USD")
        mxn = self.env.ref("base.MXN")
        for line in self:
            line.total_replacement_cost = 0
            if not (line.move_id and line.move_id.name and line.move_id.invoice_date):
                continue
            if line.move_id.currency_id:
                cost_dollar = line.product_id.quotation_cost * line.quantity
                converted = usd._convert(
                    cost_dollar, mxn, line.move_id.company_id, line.move_id.invoice_date
                )
                line.total_replacement_cost = converted
