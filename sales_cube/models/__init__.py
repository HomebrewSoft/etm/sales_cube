# -*- coding: utf-8 -*-
from . import account_move_line
from . import sales_cube_wizard
from . import res_partner
from . import product_template
