# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class SalesCubeWizard(models.Model):
    _inherit = "res.partner"

    user_sac_id = fields.Many2one(
        comodel_name="res.users",
        string=("Ejecutivo sac"),
    )
    user_quotation_id = fields.Many2one(
        comodel_name="res.users",
        string=("Ejecutivo cotizador"),
    )

    def open_mrp_production_filtered(self):
        return {
            'type': 'ir.actions.act_window',
            'name': _(f'Manufacturing Orders of {self.name}'),
            "res_model": "mrp.production",
            "views": [[self.env.ref("sales_cube.mrp_production_custom_tree_view").id, "tree"], [self.env.ref("mrp.mrp_production_form_view").id, "form"]],
            "domain": [["client_id", "=", self.id]],
        }
    
    def open_product_template_filtered(self):
        return {
            'type': 'ir.actions.act_window',
            'name': _(f'Products of {self.name}'),
            "res_model": "product.template",
            "views": [[self.env.ref("sales_cube.product_template_custom_tree_view").id, "tree"], [False, "form"]],
            "domain": [["partner_id", "=", self.id]],
        }
